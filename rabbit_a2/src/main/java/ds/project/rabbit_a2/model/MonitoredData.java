package ds.project.rabbit_a2.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


public class MonitoredData {
    int id;
    String startTime;
    String endTime;
    String activity;

    public MonitoredData(@JsonProperty("patient_id") int id,
                         @JsonProperty("activity") String activity,
                         @JsonProperty("start") String startTime,
                         @JsonProperty("end") String endTime) {
        super();
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public MonitoredData() {
        super();
        // TODO Auto-generated constructor stub
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String dateToString(Date d) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        return format.format(d);
    }

    @Override
    public String toString() {
        return  "Patient_id: "+ id +
                " Activity: " + getActivity() +
                " Start time: " + getStartTime() +
                " End time: " + getEndTime();

    }

    /**
     *
     * Show Detail View
     */
    public String toStringJSON(){
        JSONObject jsonInfo = new JSONObject();

        try {
            jsonInfo.put("patient_id", this.id);
            jsonInfo.put("activity", this.activity);
            jsonInfo.put("start", this.startTime);
            jsonInfo.put("end", this.endTime);
        } catch (JSONException e1) {}
        return jsonInfo.toString();
    }

}
