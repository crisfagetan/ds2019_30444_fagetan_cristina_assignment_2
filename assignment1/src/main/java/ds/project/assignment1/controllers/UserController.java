package ds.project.assignment1.controllers;

import ds.project.assignment1.dtos.DoctorViewDTO;
import ds.project.assignment1.dtos.UserForLoginDTO;
import ds.project.assignment1.dtos.UserForRegisterDTO;
import ds.project.assignment1.dtos.UserForViewDTO;
import ds.project.assignment1.entities.Doctor;
import ds.project.assignment1.entities.User;
import ds.project.assignment1.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService  userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/{id}")
    public UserForViewDTO findById(@PathVariable("id") Integer id){
        return userService.findUserById(id);
    }

    @PostMapping(value = "/login")
    public UserForViewDTO login(@RequestBody UserForLoginDTO userForLoginDTO){
        return userService.login(userForLoginDTO.getUsername(), userForLoginDTO.getPassword());
    }

    @GetMapping()
    public List<UserForViewDTO> findAll(){
        return userService.findAll();
    }

    @PostMapping(value = "/register")
    public UserForViewDTO registerUser(@RequestBody UserForRegisterDTO userDTO){
        int id = userService.insert(userDTO);

        return userService.findUserById(id);
    }

    @PutMapping()
    public Integer updateUser(@RequestBody UserForRegisterDTO userDTO) {
        return userService.update(userDTO);
    }

    @PutMapping(value = "/delete")
    public void delete(@RequestBody UserForViewDTO userDTO){
        userService.delete(userDTO);
    }

}
