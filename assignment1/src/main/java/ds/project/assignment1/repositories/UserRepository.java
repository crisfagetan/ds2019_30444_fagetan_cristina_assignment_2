package ds.project.assignment1.repositories;

import ds.project.assignment1.dtos.UserForViewDTO;
import ds.project.assignment1.entities.Drug;
import ds.project.assignment1.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);

//    @Query(value = "SELECT new ds.project.assignment1.dtos.UserForViewDTO(u.id, u.username, u.role) " +
//            "FROM User u " +
//            "WHERE u.username = username AND u.password = password")
//    UserForViewDTO login(String username, String password);

    public List<User> findAllByIsRemoved(Boolean isRemoved);
    public User findUserByIdAndIsRemoved(int id, Boolean isRemoved);
    public User findUserByUsernameAndPassword(String username, String password);
}
