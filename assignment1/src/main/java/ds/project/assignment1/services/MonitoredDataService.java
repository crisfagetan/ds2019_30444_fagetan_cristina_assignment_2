package ds.project.assignment1.services;

import ds.project.assignment1.dtos.DrugDTO;
import ds.project.assignment1.entities.Drug;
import ds.project.assignment1.entities.MonitoredData;
import ds.project.assignment1.errorhandlers.ResourceNotFoundException;
import ds.project.assignment1.repositories.DrugRepository;
import ds.project.assignment1.repositories.MonitoredDataRepository;
import ds.project.assignment1.validators.DrugFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MonitoredDataService {

    private final MonitoredDataRepository monitoredDataRepository;

    @Autowired
    public MonitoredDataService(MonitoredDataRepository monitoredDataRepository) {
        this.monitoredDataRepository = monitoredDataRepository;
    }

    //-------------------------------------------- FIND ----------------------------------------------------------------

    public List<MonitoredData> findAll(){
        List<MonitoredData> m = monitoredDataRepository.findAll();

        return m;
    }

//    public DrugDTO findDrugById(Integer id){
//        Drug drug  = monitoredDataRepository.findDrugByIdAndIsRemoved(id, false);
//
//        if (drug == null) {
//            throw new ResourceNotFoundException("Drug", "user id", id);
//        }
//        return DrugDTO.generateDTOFromEntity(drug);
//    }
//
//    public Drug findDrugById2(Integer id){
//        Drug drug  = monitoredDataRepository.findDrugByIdAndIsRemoved(id, false);
//
//        if (drug == null) {
//            throw new ResourceNotFoundException("Drug", "user id", id);
//        }
//        return drug;
//    }

    //-------------------------------------------- CREATE ----------------------------------------------------------------

    public Integer insert(MonitoredData monitoredData) {

        return monitoredDataRepository
                .save(monitoredData)
                .getId();
    }







}
