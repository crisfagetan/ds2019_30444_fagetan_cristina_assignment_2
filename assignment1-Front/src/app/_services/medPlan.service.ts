import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MedPlan } from '../_models/medPlan';
import { MedPlanAdd } from '../_models/medPlanAdd';

@Injectable({
  providedIn: 'root'
})
export class MedPlanService {
  baseUrl = environment.apiUrl;
  doctorId: any;
  patientId: any;
  constructor(private http: HttpClient) { }

    getMedPlans(id): Observable<MedPlan[]> {
      return this.http.get<MedPlan[]>(this.baseUrl + '/patient/' + id + '/medPlans');
    }

    insertMedPlan(medPlanAdd: MedPlanAdd) {
      this.doctorId = localStorage.getItem('userId');
      this.patientId = localStorage.getItem('patientId');
      return this.http.post(this.baseUrl + '/doctor/' + this.doctorId + '/' + this.patientId + '/medPlan', medPlanAdd);
    }
}
