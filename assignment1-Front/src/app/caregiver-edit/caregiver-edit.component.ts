import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Caregiver } from '../_models/caregiver';
import { CaregiverService } from '../_services/caregiver.service';
import { AlertifyService } from '../_services/alertify.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-caregiver-edit',
  templateUrl: './caregiver-edit.component.html',
  styleUrls: ['./caregiver-edit.component.css']
})
export class CaregiverEditComponent implements OnInit {
  editForm: NgForm;
  caregiver: Caregiver;
  drugToInsert: any = {};
  isInsert: boolean;

constructor(private caregiverService: CaregiverService, private alertify: AlertifyService,
            private route: ActivatedRoute, public router: Router) { }


  ngOnInit() {
    if (this.router.url === '/caregiver/insert') {
      this.isInsert = true;
    } else {
      this.isInsert = false;
      this.loadCaregiver();
    }
  }

  loadCaregiver() {
  // + is to be passed as a number instead of a string
    this.caregiverService.getCaregiver(+this.route.snapshot.paramMap.get('id')).subscribe((caregiver: Caregiver) => {
      this.caregiver = caregiver;
    }, error => {
      this.alertify.error(error);
    });
}

  updateCaregiver() {
    this.caregiverService.updateCaregiver(this.caregiver).subscribe(next => {
      this.alertify.success('Caregiver updated successfully');
      this.router.navigateByUrl('/caregiver');
    }, error => {
      this.alertify.error(error);
    });
  }

}
