import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertifyService } from '../_services/alertify.service';
import { DrugService } from '../_services/drug.service';
import { Drug } from '../_models/drug';
import { Router } from '@angular/router';

@Component ({
  selector: 'app-drug',
  templateUrl: './drug.component.html',
  styleUrls: ['./drug.component.css']
})
export class DrugComponent implements OnInit {
  drugs: Drug[];
  constructor(private drugService: DrugService, private alertify: AlertifyService, public router: Router) { }

  ngOnInit() {
    this.showDrugs();
  }

  showDrugs() {
    this.drugService.getDrugs().subscribe((drugs: Drug[]) => {
      this.drugs = drugs;
    }, error => {
      this.alertify.error(error);
    });
  }

  deleteRow(drug) {
    this.drugService.deleteDrug(drug).subscribe(next => {
      this.alertify.success('Drug deleted successfully');
      this.showDrugs();
    }, error => {
      this.alertify.error(error);
    });
}


}
