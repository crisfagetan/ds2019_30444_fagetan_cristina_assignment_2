import { Drug } from './drug';
import { Time } from '@angular/common';
import { Doctor } from './doctor';

export interface MedPlan {

  id: number;

  doctor: Doctor;

  drugs?: Drug[];

  intakeCount?: number;

  intakeInterval?: number;

  startHour?: Time;

  startDate?: Date;

  endDate?: Date;
}
